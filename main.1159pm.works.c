/*===============================  NOTES  ==================================*\
|     null is the only character not allowed in a filename on a unix system,  |
| so it's perfect here.                                                       |
|     This logic is even valid on windows-like systems (basically DOS, Win32, |
| and ReactOS), because FAT filesystems disallow 0x00 thru 0x1F (null and     |
| ASCII control signals). NTFS does the same except for in 'POSIX mode', which|
| still disallows 0x00 (but allows 0x01 thru 0x1F). Of course, there are      |
| probably fewer than twelve people alive who have ever used NTFS in "POSIX"  |
| mode for anything but lulz, so this is pretty irrelevant either way.        |
|                                                                             |
| I'm ignoring IBM (EBCDIC) because I don't think I'd enjoy being tortured,   |
| and I trust a COBOL programmer to know exactly what to do on such dinosaurs.|
\*==========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
int countOccurrences(const char* filename, const int target);
void printHelp(int argc, char **argv);
int checkExists(char *filename);
int copy(const char *src_file, const char *dest_file);
int main(int argc, char **argv)
{
  /* "help" printout stuff */
  if((argc < 3) || (argc > 4)){
    /* impossible to operate on given arguments, so give a handy-dandy 'help'
       output. Print to stderr unless argv[1]=="help", "-h", or "--help". In
       these cases, print to stdout so it's easier to grep.

       sprintf() used with string concatenation magic in the source code to
       make the text more legible in source form.*/
    printHelp(argc, argv);
    /* printHelp() contains exit points that should cover all possibilities.*/
    /* not rigorously mathematically proven, but even Dijkstra said that it
       was silly to expect programmers to rigorously prove their code correct.
       Don't remember what EWD paper that was, but it was one of the
       handwritten ones, so it was later on in his life.
    */
  } /* end of "help" printout stuff */



  /* count occurrences has 3 args and is the only thing that does.*/
  else if(argc==4) /* 3 + prog name */
  {
    if(strlen(argv[1])==5)
    {
      if(strncmp(argv[1],"count",5) == 0)
      {
        /* count occurrences of string in file. */
        if(strlen(argv[2]) > 0) /* Validate file arg. Prob. can't be zero, but I'm tired and it can't hurt*/
        {
          if(checkExists(argv[2]))
          {
            /* file exists and fopen() will work; validate next arg */
            
            if(strlen(argv[3]) > 0)
            {
              /* validate number arg */
              int i=0;
              int j=strlen(argv[3]);
              printf("strlen: %d\n",j);
              while(i<j)
              {
                if((!isdigit(argv[3][i])) && argv[3][i] != '-') /* isdigit() returns zero when not a digit */
                {
                  fprintf(stderr,"Error: non-numeric input for number search detected: %c\n",argv[3][i]);
                  exit(-1);
                }
                i++;
              }
              /* if we get here, argv[3] SHOULD contain a numeric value in string form. atoi() time. */
              int num_search=atoi(argv[3]);
              int count_return=countOccurrences(argv[2], num_search);
              printf("Found the number (%d) %d times\n",num_search,count_return);
            }
            
          }
          else
          {
            fprintf(stderr,"Error: could not open the file %s for reading.\nMake sure it exists, is typed correctly, and that you have permission to access it.",argv[2]);
            exit(-1);
          }
        }
        else /* I don't think this can happen, but better safe than sorry */
        {
          fprintf(stderr,"Please give a valid file name (non-null)!\n(How did you even do that?)\n");
          exit(-1);
        }
      } 
    }
    else
    { /* if command isn't 'count' */
      if(strncmp(argv[1],"copy",4) == 0 && strlen(argv[1]) == 4 )
      {
        if(strnlen(argv[2],16) > 15 || strlen(argv[2]) < 1)
        {
          fprintf(stderr,"Error: filenames must be given and be under 16 characters.\n");
          exit(-1);
        }
        /* running close to deadline. duplicate code here. Sorry. */
        if(strnlen(argv[3],16) > 15 || strlen(argv[3]) < 1)
        {
          fprintf(stderr,"Error: filenames must be given and be under 16 characters.\n");
          exit(-1);
        }
        /* else we are good to start the copy*/
        copy(argv[2], argv[3]);
      }
    }
    
  }
}



int countOccurrences(const char* filename, const int target)
{
  int instances=0;
  FILE *file=fopen(filename,"rb");
  if(file==NULL)
  {
    fprintf(stderr,"Wow, you're fast! In between the time it took to check that\n'%s' existed and now, you deleted the file!!\n(By the way, that's an error. Exiting.)\n",filename);
    exit(-1);
  }
  
  /* re-inventing the wheel. Enjoy! */
  int chr=0; /* character temp. storage */
  /* start getting chars and converting to numbers. Ignore non-numerical values and
     require whitespace delimiting to parse.*/
  int newNumberPossible=1; /* 1=true. 0=false. The first char in the file doesn't
                             have to be preceded by a space; only further digits
                             which are not themselves preceded by other digits.*/
  int newDigitPossible=1;  /* additional digits after the first digit in a number. */
  long numStartPos=ftell(file); /* used to keep track of the position of the
                                beginning of the number currently being
                                processed. Useful for determining malloc() size
                                dynamically. */
  int curNumLength=0;
  int sign=1; /* 1 or -1. multiplier. */
  
  while((chr=fgetc(file)) != EOF)
  {
    if(newNumberPossible==0)
    {
      if(newDigitPossible) /* if we are in the middle of a number */
      {
        if(isdigit(chr))
        {
          curNumLength++;
        }
        else
        {
          newDigitPossible=0;
          newNumberPossible=1;
          /* we have run over the duration of the number with a leading space. */
          /* is there a trailing space? If not, it's junk data. */
          if(isspace(chr))
          {
            /* we've got a match! */
            /* allocate enough room for the string so we can atoi() it */
            int *numStr=calloc(curNumLength + 1, sizeof(int));
            /* go back to the beginning of the number */
            fseek(file,numStartPos,SEEK_SET);
            /* go back one to check for a sign */
            fseek(file,-1,SEEK_CUR);
            if(fgetc(file)=='-')
            {
              sign=-1;
            }
            else
            {
              sign=1;
            }
            int i=0;
            while(i<curNumLength)
            {
              numStr[i]=fgetc(file);
              i++;
            }
            numStr[i]='\0'; /* null terminator! */
            /* spent waayy too long on this. This bit is redundant because I'm getting bored */
            /* convert int array to char array so atoi() handles it better */
            char *numStrChr=calloc(curNumLength + 1, sizeof(char));
            i=0;
            while(i<curNumLength)
            {
              numStrChr[i]=(char)numStr[i];
              i++;
            }
            numStrChr[i]='\0';
            int newNum=atoi(numStrChr) * sign; /* don't forget the signing bit */
            printf("\t\tFound num:\t%d\n",newNum);
            free(numStr);
            free(numStrChr);
            if(newNum==target)
            {
              instances++;
            }
            /* we should be "seeked" back to the end of the number now, ready to continue */
          }
          else
          {
            newDigitPossible=0;
            newNumberPossible=0; /* next char guaranteed not to be a valid number */
          }
        }
      }
      if(isspace(chr))
      {
        /* we've hit a space character, so the next time through we might hit
           the start of a new number! */
        newNumberPossible=1;
        numStartPos=ftell(file);
        char tmpchr=fgetc(file);
        if(tmpchr == '-') /* peek ahead for sign */
        {
          sign=-1;
          numStartPos=ftell(file);
          /* don't seek back if it's negative */
        }
        else
        {
          sign=1; /* don't lose track of that sign. */
          ungetc(tmpchr,file); /* roll back */
        }

                                 /* have to do it here because fgetc() has already
                                 moved the pointer forward one from this offset
                                 in the file. */
      }
      /* else we keep going. (loop will go to next char) */
    }
    else /* if we could find a new number here */
    {
      if(isspace(chr))
      {
        numStartPos=ftell(file); /* update starting address */
      }
      else if(chr=='-')
      {
        char tmpchr=fgetc(file);
        if(isdigit(tmpchr)) /* peek ahead. is a digit next? */
        {
          sign=-1; /* negative number. Also update numStartPos. */
          newNumberPossible=1;
          ungetc(tmpchr,file); /* roll back */
        }
        else
        {
          ungetc(chr,file); /* roll back */
        }
        numStartPos=ftell(file);
      }
      else if(isdigit(chr))
      {
        curNumLength=1;
        newDigitPossible=1;
        newNumberPossible=0;
      }
    }
  }
  fclose(file);
  return instances;
}


void printHelp(int argc, char **argv)
{
/*
  was originally written inside the main() function. Since it already needs
  access to argv[0], it makes sense to just pass it the values straight from
  main().
*/
  char helpBody[]=
    "Usage: %s [command] [args]\n"
    "\n"
    "Valid commands are:\n"
    "    copy [source] [destination]\n"
    "        Copy file [source] to file [destination], overwriting existing files.\n"
    "        does not preserve file permissions or other attributes a la the 'cp'\n"
    "        command, and is likely slower due to copying files one byte at a time.\n"
    "    count [filename] [number]\n"
    "        Count the number of times [number] occurs in the file [filename].\n"
    "        Filenames are limited to 15 characters.\n"
    "    help (aliased to '-h', '--help' for familiarity)\n"
    "        Display this help message, printed on standard output.\n";
  /* put this in the string located at 'help' */
  int helpTotalLength=strlen(helpBody) + strlen(argv[0]) - 1;
  /* %s is two characters that will be substituted out later, but we need to
     add one to that for the null terminator byte. so we only subtract one.*/
  
  /* now that we've calculated how big our help text should be, time to
     allocate, print, and free. */
  char *help=malloc(helpTotalLength);
  sprintf(help,
          helpBody,
          argv[0]
    );
  /* sprintf() automatically null terminates */
  /* strncmp() returns zero if the strings match, so we need to do strncmp()==0. */
  if(argc>1) /* don't check for arguments if none were passed */
  {
    /* strlen needed because otherwise any string starting with '-h' for instance would be considered a valid command */
    if( (strncmp(argv[1],"help",4) == 0 && strlen(argv[1]) == 4) || ( strncmp(argv[1],"--help",6) == 0 && strlen(argv[1]) == 6) || ( strncmp(argv[1],"-h",2) == 0 && strlen(argv[1]) == 2) ) /* stdout*/
    {
      printf("%s",help);
      free(help);
      exit(0);
    }
    else
    {
      goto helpStderr; /* MUAHAHAHAHAHAHAHAHA */
      /* if you want to dock me, fine. But it works here and the alternative
         is duplicated code. Either I copy-paste the fprintf() that I used
         for the case where no arguments were passed, or I break it out
         to a function and pass a pointer to my malloc'd string, OR I just
         add this one line for error handling, which goes to a place where
         the program will free what it's malloc'd and exit, with no further
         things to mess up possible. This is goto for error handling, and
         if it makes you feel any better, kernel programmers use it regularly.
         
         Someone else's nice justification: see the accepted answer to
         https://softwareengineering.stackexchange.com/questions/154974/is-this-a-decent-use-case-for-goto-in-c
      */
    }
  }
  else /* stderr*/
  {
  helpStderr:
    fprintf(stderr,"%s",help);
    free(help);
      exit(-1);
  }
  exit(0);
}

int checkExists(char *filename) /* returns 0 if not found, 1 otherwise */
{
  FILE *file=fopen(filename,"rb");
  if(file == NULL) /* if file not found */
  {
    fprintf(stderr,"Error: File %s could not be opened.",filename);
    return 0;
  }
  /* implicit else */
  fclose(file);
  return 1;
}

int copy(const char *src_file, const char *dest_file)
{
/* the lengths of the filenames are checked in main(). Due to time constraints I can't
   fix that at this moment. It's still perfectly safe though. (11:34PM)*/
  FILE *inFile=fopen(src_file,"rb");
  FILE *outFile=fopen(dest_file,"w+");
  if(outFile==NULL)
  {
    if(inFile!=NULL)
    {
      fclose(inFile);
    }
    fprintf(stderr,"copy: Output file could not be opened, exiting.\n");
    exit(-1);
  }
  else if(inFile==NULL)
  {
    if(outFile!=NULL)
    {
      fclose(outFile);
    }
    fprintf(stderr,"copy: Input file could not be opened, exiting.\n");
    exit(-1);
  }
  int chr;
  while((chr=fgetc(inFile)) != EOF)
  {
    fputc(chr,outFile);
  }
  fclose(outFile);
  fclose(inFile);
  printf("File %s copied to %s successfully.",src_file,dest_file);
  return 0;
}

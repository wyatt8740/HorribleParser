# Horrible Parser in C

I had a great teacher in a C and Unix class I took for my major, and I went
into the class already knowing C and Unix, so I had a little fun and went off
the rails with this assignment.

My prof. said something along the lines of it being like watching a
train crash: it was horrible, but he couldn't look away.

Don't do stuff like this unless you're confident that you'll be getting laughs
in response to it, rather than getting killed.

![Original assignment sheet](HW5_232_Spring18.png)
